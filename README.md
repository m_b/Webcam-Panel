# Webcam-Panel

Little webcam settings panel.

OS: GNU/Linux

## Why

While making stop motion clips with Bruno Levefre's beautiful FOSS „Heron Animation“ I miss a little config panel for my webcam, a Logitech c920.  
Here it is. Maybe it's useful for someone else.

## Copy and run

Install V4l-Utils if needed:
```sh
sudo apt update && sudo apt upgrade -y
sudo apt install v4l-utils
```

Download
```sh
curl https://codeberg.org/m_b/Webcam-Panel/archive/main.tar.gz | tar -xvz
```

Run
```sh
cd webcam_settings/app/
cmod +x webcam_settings
./webcam_settings
```

Application Starter
```sh
mkdir -p ~/.icons ~/.local/share/applications ~/.local/bin
cp webcam_settings ~/.local/bin/
cp webcam_settings.png ~/.icons/
cp webcam_settings.desktop ~/.local/share/applications/
```
